using System.ComponentModel.DataAnnotations;

namespace DatingApp.API.Dtos
{
    public class UserForRegisterDto
    {
        [Required(ErrorMessage = "Username is required dummy")]
        public string Username { get; set; }
        [Required]
        public string Password { get; set; }
    }
}